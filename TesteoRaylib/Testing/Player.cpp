#include "pch.h"
#include "Player.h"
namespace PONG
{
	namespace Player
	{
		Players rectangle1;
		Players rectangle2;
		void initPlayer()
		{
			rectangle1.score = 0;
			rectangle2.score = 0;

			rectangle1.rectangleWidth = 23;
			rectangle1.rectangleHeight = 100;

			rectangle2.rectangleWidth = 23;
			rectangle2.rectangleHeight = 100;

			rectangle1.rectangle = { GetScreenWidth() - rectangle1.rectangleWidth - 10, static_cast<float>(GetScreenHeight() / 2),
									 rectangle1.rectangleWidth,rectangle1.rectangleHeight };
			rectangle2.rectangle = { 10, static_cast<float>(GetScreenHeight() / 2), rectangle2.rectangleWidth, rectangle2.rectangleHeight };
			rectangle1.goUp = KEY_W;
			rectangle1.goDown = KEY_S;
			rectangle2.goUp = KEY_O;
			rectangle2.goDown = KEY_L;
		}
		void playerMovement(Players &rectangle1, Players &rectangle2)
		{
			if (IsKeyDown(rectangle1.goUp) && rectangle1.rectangle.y > 10)
			{
				rectangle1.rectangle.y -= 4.0f;
			}
			if (IsKeyDown(rectangle1.goDown) && rectangle1.rectangle.y < GetScreenHeight() - rectangle1.rectangleHeight - 10)
			{
				rectangle1.rectangle.y += 4.0f;
			}

			if (IsKeyDown(rectangle2.goUp) && rectangle2.rectangle.y > 10)
			{
				rectangle2.rectangle.y -= 4.0f;
			}
			if (IsKeyDown(rectangle2.goDown) && rectangle2.rectangle.y < GetScreenHeight() - rectangle1.rectangleHeight - 10)
			{
				rectangle2.rectangle.y += 4.0f;
			}

		}
		void draw(Players &rectangle1, Players &rectangle2)
		{		
			DrawRectangleRec(rectangle1.rectangle, BLUE);
			DrawRectangleRec(rectangle2.rectangle, RED);
		}
	}
}
