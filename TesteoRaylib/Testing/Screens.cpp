#include "pch.h"
#include "Screens.h"
#include <iostream>
using namespace std;

namespace PONG
{
	
	namespace Screens
	{
		State state;
		MiddleLine middleLine;
		int controlToSelect;
		void initGame()
		{
			middleLine.thickness = 10;
			state = mainmenu;
			InitWindow(screenWidth, screenHeight, "abrite pls");
			middleLine.startPos = { static_cast<float>(GetScreenWidth() / 2), 0 };
			middleLine.endPos = { static_cast<float>(GetScreenWidth() / 2),static_cast<float>(GetScreenHeight()) };
			controlToSelect = 0;
			SetTargetFPS(60);
		}
		void gameplay(Ball::Ball &ball, Player::Players &rectangle1, Player::Players &rectangle2, int &colisiono,State &state)
		{
			playerMovement(rectangle1, rectangle2);

			onCollission(ball, rectangle1, rectangle2, colisiono);

			ballMovement(ball);

		}
		void menuInteraction(State &state)
		{
			if (IsKeyPressed(KEY_ENTER)) { state = playing; }

			if (IsKeyPressed(KEY_C)) { state = configuration; }

			if (IsKeyPressed(KEY_E)) { state = exit; }

			if (IsKeyPressed(KEY_P)) { state = pause; }

			if (IsKeyPressed(KEY_M)) { state = mainmenu; }

			if (IsKeyPressed(KEY_J)) { state = controls; }
			
		}
		void draw(MiddleLine &middleLine, Player::Players &rectangle1, Player::Players &rectangle2)
		{		
			DrawLineEx(middleLine.startPos, middleLine.endPos, middleLine.thickness, BLACK);
			DrawText(FormatText("Score Pj1 : %i ", rectangle1.score), GetScreenWidth() / 6, 0, 20, BLACK);
			DrawText(FormatText("Score Pj2 : %i ", rectangle2.score), GetScreenWidth() / 2 + GetScreenWidth() / 4, 0, 20, BLACK);
			if (state == pause) { DrawText("Pause", (GetScreenWidth() / 2) - 100, (GetScreenHeight() / 2), 40, BLACK); }	
		}
		void update() 
		{
			initGame();
			Ball::initBall();
			Player::initPlayer();
			while (!WindowShouldClose())    // Detect window close button or ESC key
			{
				menuInteraction(state);
				ClearBackground(RAYWHITE);
				BeginDrawing();
				switch (state)
				{
				case playing:
					gameplay(Ball::ball, Player::rectangle1, Player::rectangle2, Ball::itCollided,state);
					Ball::draw(Ball::ball);
					draw(middleLine, Player::rectangle1, Player::rectangle2);
					Player::draw(Player::rectangle1,Player::rectangle2);
					if (Player::rectangle1.score >= scoreAmount)
					{
						state = victory1;
					}
					else if (Player::rectangle2.score >= scoreAmount)
					{
						state = victory2;
					}
					break;
				case mainmenu:
					Ball::restart(Ball::ball, Player::rectangle1, Player::rectangle2);
					Player::rectangle1.score = 0;
					Player::rectangle2.score = 0;
					DrawText("Pong", (GetScreenWidth() / 2) - 100, (GetScreenHeight() / 2) - 100, 50, BLUE);
					DrawText("Press [ENTER] to Start", (GetScreenWidth() / 2) - 100, (GetScreenHeight() / 2), 20, BLACK);
					DrawText("Press [C] to Config", (GetScreenWidth() / 2) - 100, (GetScreenHeight() / 2) + 50, 20, BLACK);
					DrawText("Press [J] to Controls", (GetScreenWidth() / 2) - 100, (GetScreenHeight() / 2) + 100, 20, BLACK);
					DrawText("Press [ESC] to Exit", (GetScreenWidth() / 2) - 100, (GetScreenHeight() / 2) + 150, 20, BLACK);
					break;
				case configuration:
					DrawText("Config", (GetScreenWidth() / 2) - 100, (GetScreenHeight() / 2) - 100, 50, BLUE);
					selectControls();
					
					break;
				case controls:
					DrawText("Controls", (GetScreenWidth() / 2) - 100, (GetScreenHeight() / 2) - 100, 50, BLUE);
					DrawText(FormatText("Player 1 move up : %c ", (char)Player::rectangle1.goUp), (GetScreenWidth() / 2) - 100, (GetScreenHeight() / 2), 20, BLACK);
					DrawText(FormatText("Player 1 move down : %c ", (char)Player::rectangle1.goDown), (GetScreenWidth() / 2) - 100, (GetScreenHeight() / 2) + 50, 20, BLACK);
					DrawText(FormatText("Player 2 move up : %c ", (char)Player::rectangle2.goUp), (GetScreenWidth() / 2) - 100, (GetScreenHeight() / 2) + 100, 20, BLACK);
					DrawText(FormatText("Player 2 move down : %c ", (char)Player::rectangle2.goDown), (GetScreenWidth() / 2) - 100, (GetScreenHeight() / 2) + 150, 20, BLACK);
					break;
				case pause:
					DrawText("Pause", (GetScreenWidth() / 2) - 100, (GetScreenHeight() / 2) - 100, 50, BLUE);
					DrawText("Press [M] to go back to Menu", (GetScreenWidth() / 2) - 100, (GetScreenHeight() / 2), 20, BLACK);
					DrawText("Press [ESC] to Exit game", (GetScreenWidth() / 2) - 100, (GetScreenHeight() / 2) + 50, 20, BLACK);
					break;
				case victory1:
					DrawText("Player 1 has won!", (GetScreenWidth() / 2) - 350, (GetScreenHeight() / 2), 80, BLUE);
					DrawText("Press [ESC] to leave", (GetScreenWidth() / 2) - 200, (GetScreenHeight() / 4), 40, BLUE);
					break;
				case victory2:
					DrawText("Player 2 has won!", (GetScreenWidth() / 2) - 350, (GetScreenHeight() / 2), 80, BLUE);
					DrawText("Press [ESC] to leave", (GetScreenWidth() / 2) - 200, (GetScreenHeight() / 4), 40, BLUE);
					break;
				default:
					break;
				}
				EndDrawing();
				//----------------------------------------------------------------------------------
			}
			CloseWindow();        // Close window and OpenGL context
		}
		void selectControls() 
		{
			switch (controlToSelect)
			{
			case 0:
				DrawText("J1 Ingrese tecla mover hacia arriba", (GetScreenWidth() / 2) - 100, (GetScreenHeight() / 2), 20, BLACK);
				if (GetKeyPressed() != KEY_P && GetKeyPressed() != KEY_M && GetKeyPressed() != KEY_C && GetKeyPressed() != KEY_ENTER && GetKeyPressed() != KEY_E)
				{
					controlToSelect++;
					Player::rectangle1.goUp = GetKeyPressed();
				}
				cout << "a" << endl;
				break;
			case 1:
				DrawText("J1 Ingrese tecla mover hacia abajo", (GetScreenWidth() / 2) - 100, (GetScreenHeight() / 2), 20, BLACK);
				if (GetKeyPressed() != KEY_P && GetKeyPressed() != KEY_M && GetKeyPressed() != KEY_C &&
					GetKeyPressed() != KEY_ENTER && GetKeyPressed() != KEY_E && GetKeyPressed() != Player::rectangle1.goDown)
				{
					controlToSelect++;
					Player::rectangle1.goDown = GetKeyPressed();
				} 
				break;
			case 2:
				DrawText("J2 Ingrese tecla mover hacia arriba", (GetScreenWidth() / 2) - 100, (GetScreenHeight() / 2), 20, BLACK);
				if (GetKeyPressed() != KEY_P && GetKeyPressed() != KEY_M && GetKeyPressed() != KEY_C &&
					GetKeyPressed() != KEY_ENTER && GetKeyPressed() != KEY_E && GetKeyPressed() != Player::rectangle1.goDown &&
					GetKeyPressed() != Player::rectangle1.goUp)
				{
					controlToSelect++;
					Player::rectangle2.goUp = GetKeyPressed();
				}
				break;
			case 3:
				DrawText("J2 Ingrese tecla mover hacia abajo", (GetScreenWidth() / 2) - 100, (GetScreenHeight() / 2), 20, BLACK);
				if (GetKeyPressed() != KEY_P && GetKeyPressed() != KEY_M && GetKeyPressed() != KEY_C &&
					GetKeyPressed() != KEY_ENTER && GetKeyPressed() != KEY_E && GetKeyPressed() != Player::rectangle1.goDown &&
					GetKeyPressed() != Player::rectangle1.goUp && GetKeyPressed() != Player::rectangle2.goUp)
				{
					controlToSelect++;
					Player::rectangle2.goDown = GetKeyPressed();
				}
				break;
			default:
				break;
			}
		}
	}
}
