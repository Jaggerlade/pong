#ifndef Player_h
#define Player_h
#include "raylib.h"
#define scoreAmount  3
namespace PONG {
	namespace Player {

		struct Players
		{
			Rectangle rectangle;
			float rectangleWidth;
			float rectangleHeight;
			int score;
			int goUp;
			int goDown;
		};
		extern Players rectangle1;
		extern Players rectangle2;

		void playerMovement(Players &rectangle1, Players &rectangle2);
		void initPlayer();
		void draw(Players &rectangle1, Players &rectangle2);
	}

}	 
#endif // !Player_h
