#include "pch.h"
#include "Ball.h"
#include <iostream>

namespace PONG 
{

	namespace Ball
	{
		Ball ball;
		int itCollided;
		bool increaseSpeed;
		void initBall()
		{
			
			ball.ballRadious = 25;
			ball.ballPosition = { static_cast<float>(GetScreenWidth() / 2), static_cast<float>(GetScreenHeight() / 2 )};
			ball.ballSpeed = { 4.0f, 3.0f };
			itCollided = 0;
			ball.maxSpeed = 10.0f;
			increaseSpeed = false;
			ball.minDirection = -4.0f;
			ball.maxDirection = 4.0f;

		}
		void ballMovement(Ball &ball)
		{
			
			ball.ballPosition.x -= ball.ballSpeed.x;
			ball.ballPosition.y -= ball.ballSpeed.y;

		}
		void onCollission(Ball &ball, Player::Players &rectangle1, Player::Players &rectangle2, int &colisiono)
		{
			if (ball.ballSpeed.x <= ball.maxSpeed && ball.ballSpeed.x >= -ball.maxSpeed){increaseSpeed = true;}
			else{increaseSpeed = false;}

			if (CheckCollisionCircleRec(ball.ballPosition, ball.ballRadious, rectangle1.rectangle) && colisiono != 1)
			{
				colisiono = 1;
				ball.ballSpeed.x *= -1.0f;
				if (increaseSpeed == true)
				{
					ball.ballSpeed.x += 0.6f;
				}
			}
			if (CheckCollisionCircleRec(ball.ballPosition, ball.ballRadious, rectangle2.rectangle) && colisiono != 2)
			{
				colisiono = 2;
				ball.ballSpeed.x *= -1.0f;
				if (increaseSpeed == true)
				{
					ball.ballSpeed.x -= 0.6f;
				}
			}
			if (((ball.ballPosition.y + ball.ballRadious) >= GetScreenHeight()) ||
				((ball.ballPosition.y - ball.ballRadious) <= 0)) ball.ballSpeed.y *= -1.0f;

			if ((ball.ballPosition.x + ball.ballRadious) >= GetScreenWidth())
			{
				do
				{
					ball.direction = static_cast <float>(GetRandomValue(ball.minDirection, ball.maxDirection));
				} while (ball.direction < 2.0f && ball.direction > -2.0f);
				rectangle1.score++;
				ball.ballSpeed = { -4.0f, ball.direction };
				restart(ball, rectangle1, rectangle2);
				std::cout << ball.direction<<std::endl;
			}
			if ((ball.ballPosition.x - ball.ballRadious) <= 0)
			{
				do
				{
					ball.direction = static_cast <float>(GetRandomValue(ball.minDirection, ball.maxDirection));
				} while (ball.direction < 2.0f && ball.direction > -2.0f);
				
				rectangle2.score++;
				ball.ballSpeed = { 4.0f, ball.direction };
				restart(ball, rectangle1, rectangle2);
				std::cout << ball.direction << std::endl;
			}
		}
		void restart(Ball &ball, Player::Players &rectangle1, Player::Players &rectangle2)
		{
			ball.ballPosition = { static_cast<float>(GetScreenWidth() / 2), static_cast<float>(GetScreenHeight() / 2)};
			rectangle1.rectangle = { GetScreenWidth() - rectangle2.rectangleWidth - 10,static_cast<float>(GetScreenHeight() / 2),
									 rectangle1.rectangleWidth,rectangle1.rectangleHeight };
			rectangle2.rectangle = { 10, static_cast<float>(GetScreenHeight() / 2), rectangle2.rectangleWidth, rectangle2.rectangleHeight };
		}
		void draw(Ball &ball)
		{
			DrawCircleV(ball.ballPosition, ball.ballRadious, GREEN);
		}
	}
}