#ifndef Screens_h
#define Screens_h
#include "raylib.h"
#include "Ball.h"
#include "Player.h"
#define screenWidth 1280
#define screenHeight 720
namespace PONG 
{
	namespace Screens 
	{
		struct MiddleLine
		{
			Vector2 startPos;
			Vector2 endPos;
			float thickness;
		};
		enum State
		{
			playing,
			mainmenu,
			pause,
			configuration,
			controls,
			exit,
			victory1,
			victory2
		};
		extern MiddleLine middleLine;
		extern State state;
		extern int controlToSelect;
		
		void initGame();
		void gameplay(Ball::Ball &ball, Player::Players &rectangle1, Player::Players &rectangle2, int &colisiono,State &state);
		void menuInteraction(State &state);
		void draw(MiddleLine &middleLine, Player::Players &rectangle1, Player::Players &rectangle2);
		void update();
		void selectControls();
	}

}

#endif
