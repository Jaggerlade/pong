#ifndef Ball_h
#define Ball_h
#include "raylib.h"
#include "Player.h"
namespace PONG 
{

	namespace Ball 
	{
		extern int itCollided;
		struct Ball
		{
			Vector2 ballPosition;
			Vector2 ballSpeed;
			float ballRadious;
			float maxSpeed;
			float direction;
			float maxDirection;
			float minDirection;
		};
		extern Ball ball;
		extern bool increaseSpeed;
		void initBall();
		void ballMovement(Ball &ball);
		void onCollission(Ball &ball, Player::Players &rectangle1, Player::Players &rectangle2, int &colisiono);
		void restart(Ball &ball, Player::Players &rectangle1, Player::Players &rectangle2);
		void draw(Ball &ball);
	}
}

#endif // !Ball.h
